import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { URLS } from '../../providers/urls';

@IonicPage()
@Component({
  selector: 'page-ac-report',
  templateUrl: 'ac-report.html',
})
export class AcReportPage implements OnInit {
  datetimeStart: any;
  datetimeEnd: any;
  islogin: any;
  portstemp: any[] = [];
  dev_id: any;
  reportData: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCAll: ApiServiceProvider,
    public urls: URLS
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AcReportPage');
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = this.urls.mainURL+'/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCAll.startLoading().present();
    this.apiCAll.urlPassed(baseURLp)
      .subscribe(data => {
        this.apiCAll.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apiCAll.stopLoading();
          console.log(err);
        });
  }

  getAcReportData() {
    let that = this;
    this.reportData = [];
    if (that.dev_id == undefined) {
      that.dev_id = "";
    }
    var burl = this.urls.notifsURL + '/ACSwitchReport?from_date=' + new Date(that.datetimeStart).toISOString() + '&to_date=' + new Date(that.datetimeEnd).toISOString() + '&user=' + that.islogin._id + '&device=' + that.dev_id;
    this.apiCAll.startLoading().present();;
    this.apiCAll.urlPassed(burl)
      .subscribe(data => {
        this.apiCAll.stopLoading();
        console.log("ac report data: ", data)
        this.reportData = data;
        console.log("1stAction: ", data[0]['1stAction'])

      })
  }

  getAcReporID(key) {
    console.log("key: ", key.Device_ID)
    this.dev_id = key.Device_ID;
  }

  showDetail(pdata) {
    console.log("pdata: ", pdata);
    let that = this;
    this.navCtrl.push(ACDetailPage, {
      'param': pdata,
      'fdate': that.datetimeStart,
      'tdate': that.datetimeEnd,
      'uid': that.islogin._id
    })
  }
}

@Component({
  templateUrl: './ac-detail.html',
  styles: [`
  .col {
    padding: 0px;
}`]
})
export class ACDetailPage implements OnInit {
  fdate: any;
  tdate: any;
  uid: any;
  paramData: any = {};
  ddata: any[] = [];
  vname: any;

  constructor(
    public apiCall: ApiServiceProvider,
    public navParam: NavParams,
    public urls: URLS
  ) {
    console.log("param parameters: ", this.navParam.get('param'))
    this.paramData = this.navParam.get('param');
    this.fdate = this.navParam.get('fdate');
    this.tdate = this.navParam.get('tdate');
    this.uid = this.navParam.get('uid');
  }

  ngOnInit() {
    this.getDetails();
  }

  getDetails() {
    var bser = this.urls.notifsURL + "/acReport?from_date=" + new Date(this.fdate).toISOString() + "&to_date=" + new Date(this.tdate).toISOString() + "&_u="+this.uid+"&vname=" + this.paramData._id.imei;
    this.apiCall.startLoading().present();
    this.apiCall.urlPassed(bser)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("detailed ac report data: ", data[0].s);
        this.ddata = data[0].s;
      })
  }
}
