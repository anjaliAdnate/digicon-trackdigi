import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { NetworkInterface } from '@ionic-native/network-interface';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { URLS } from '../../../providers/urls';

@IonicPage()
@Component({
  selector: 'page-payment-secure',
  templateUrl: 'payment-secure.html',
})
export class PaymentSecurePage {
  paytmregNum: string;
  rideFare: any;
  amounttobepaid: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private networkInterface: NetworkInterface,
    public urls: URLS) {
    this.rideFare = this.navParams.get('rideFare');
    console.log(this.rideFare);
  }

  ionViewDidLoad() {
    this.amounttobepaid = JSON.parse(localStorage.getItem('tripFare'));
    console.log(this.amounttobepaid);

    this.getNetworkDetal();
  }

  proceedSecurely() {

    // PaytmwalletloginPage

    this.paytmregNum = localStorage.getItem('paytmregNum');
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    console.log(userId);
    let toastCtrl = this.toastCtrl.create({
      message: "Payment done, Thanks for using OneQlik VTS !!",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })
    let toastCtrlfailure = this.toastCtrl.create({
      message: "Payment failed , Try again",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })

    let toastCtrlServerError = this.toastCtrl.create({
      message: "Internal Server Error, Try Again",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })


    let updatePaymentStatus = this.toastCtrl.create({
      message: "Unable to update Payment Status ",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })



    var paymentWithdraw = {
      CUST_ID: userId,
      app_ip: "192.168.1.102",
      deviceId: this.paytmregNum,
      "app_id": "OneQlikVTS",
      "TXN_AMOUNT": this.amounttobepaid
    }
    var url = this.urls.mainURL + '/paytm/Withdraw';
    this.apiCall.startLoading();
    this.apiCall.urlwithPayload(url, paymentWithdraw)
      .subscribe(res => {
        this.apiCall.stopLoading();
        console.log(res);
        if (res.Status == "TXN_SUCCESS") {
          var ubr = this.urls.mainURL + '/zogo/updateZogoPaymentStatus?user=' + userId;
          this.apiCall.urlPassed(ubr).subscribe(res => {
            console.log("updateStatusResponse=>", res);
            if (res.payment_status == true) {
              toastCtrl.present()
              toastCtrl.onDidDismiss(() => {
                localStorage.removeItem('flag');
                this.navCtrl.setRoot("LivePage");
              })

            } else {

              updatePaymentStatus.present();

            }

          })

        } else if (res.STATUS == "TXN_FAILURE") {
          console.log("Transaction failed ");
          toastCtrlfailure.present();
        } else {
          toastCtrlServerError.present();
        }
      })
  }

  getNetworkDetal() {
    this.networkInterface.getCarrierIPAddress()
      .then(address => console.info(`IP: ${address.ip}, Subnet: ${address.subnet}`))
      .catch(error => console.error(`Unable to get IP: ${error}`));

  }
  toasterror(errstatus) {
    console.log(errstatus);
    let toastCtrl1 = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom',
      cssClass: 'toastStyle'
    })

    toastCtrl1['message'] = errstatus;
    console.log(toastCtrl1);
    return toastCtrl1.present();


  }

  releaseAMT() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;

    this.apiCall.startLoading();
    var releaseObj = {
      CUST_ID: userId,
      app_id: 'OneQlikVTS'
    }
    var _burl = this.urls.mainURL + '/paytm/releaseAmount';
    this.apiCall.urlwithPayload(_burl, releaseObj)
      .subscribe(res => {
        if (res.STATUS == "TXN_SUCCESS") {
          this.apiCall.stopLoading();
          this.proceedSecurely();
        } else {
          console.log("Please Try again");
          this.toasterror("try aghain");
        }

      }, err => {
        console.log("Internal server Error")
        this.apiCall.stopLoading();
        this.toasterror("try aghain");
      })
  }
}
