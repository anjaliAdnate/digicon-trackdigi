import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { URLS } from '../../../providers/urls';

@IonicPage()
@Component({
  selector: 'page-paytmwalletlogin',
  templateUrl: 'paytmwalletlogin.html',
})
export class PaytmwalletloginPage {
  paytmmail: any;
  paytmnumber: any;
  paytmotp: number;
  successresponse: boolean = false;
  inputform: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public apiCall: ApiServiceProvider,
    private menu: MenuController,
    public toast: ToastController,
    public urls: URLS
  ) {
    // debugger
    var num = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
    this.paytmnumber = num.phn;
  }
  ionViewDidEnter() {

    this.menu.enable(true);
  }

  paytmSignupOTPResponse: any;

  paytmwallet() {

    var useDetails = localStorage.getItem('details');
    var userId = JSON.parse(useDetails)._id;

    // payment gateway integration function
    var paytmUserCredentials = {
      "user": userId,
      "app_id": "OneQlikVTS",
      "phone": this.paytmnumber,
      "scope": "wallet",
      "responseType": "token",
    }
    var _baseUr = this.urls.mainURL + '/paytm/sendOTP';

    this.apiCall.urlwithPayload(_baseUr, paytmUserCredentials)
      .subscribe(res => {
        this.successresponse = true;
        this.inputform = true;
        this.paytmSignupOTPResponse = res;
        let errToast_success = this.toast.create({
          message: 'OTP sent successfully, Please type OTP in next field !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        errToast_success.present();

      }, err => {
        let errToast = this.toast.create({
          message: 'Server error , Please try after somtime !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        if (err) {
          console.log("error occured !!!");
          errToast.present();
        }
      })
  }

  goBack() {
    this.navCtrl.pop();
  }
  resndOTP() {
    var useDetails = localStorage.getItem('details');
    var userId = JSON.parse(useDetails)._id;
    var paytmUserCredentials = {
      "user": userId,
      "app_id": "OneQlikVTS",
      "phone": this.paytmnumber,
      "scope": "wallet",
      "responseType": "token"
    }
    var _burl = this.urls.mainURL + '/paytm/sendOTP';
    this.apiCall.urlwithPayload(_burl, paytmUserCredentials)
      .subscribe(res => {
        this.successresponse = true;
        this.inputform = true;
        this.paytmSignupOTPResponse = res;
        let errToast_success = this.toast.create({
          message: 'OTP sent successfully, Please type OTP in next field !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        errToast_success.present();

      }, err => {
        let errToast = this.toast.create({
          message: 'Server error , Please try after somtime !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        if (err) {
          errToast.present();
        }
      })
  }

  navOptions = {
    animation: 'ios-transition'
  };
  paytmAuthantication() {
    this.apiCall.startLoading();
    var otpCredentials = {
      "otp": this.paytmotp,
      "transac_id": this.paytmSignupOTPResponse.transac_id,
      "app_id": "OneQlikVTS"
    }
    var _baseUr = this.urls.mainURL + '/paytm/validateOTP';
    this.apiCall.urlwithPayload(_baseUr, otpCredentials)
      .subscribe(res => {
        console.log("response from wallet page=> ", res)
        this.apiCall.stopLoading();
        var s = JSON.parse(res.response)
        if (s.status == 'FAILURE') {
          let toast = this.toastCtrl.create({
            message: s.message,
            duration: 2000,
            position: "bottom"
          })

          toast.onDidDismiss(() => {
            this.paytmotp = undefined;
          });

          toast.present();
        } else {
          localStorage.setItem('paytmregNum', this.paytmnumber);
          this.navCtrl.push("WalletPage", null, this.navOptions);
        }
      }, err => {
        console.log("error found=> " + err)
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: 'internal server Error, Please try after sometime !!!',
          duration: 3000,
          position: 'top'
        });
        toast.present();
      })
  }
}
