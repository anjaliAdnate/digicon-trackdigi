import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadDocPage, DocPopoverPage } from './upload-doc';
import { ViewDoc } from './view-doc/view-doc';

@NgModule({
  declarations: [
    UploadDocPage,
    ViewDoc,
    DocPopoverPage
  ],
  imports: [
    IonicPageModule.forChild(UploadDocPage),
  ],
  entryComponents: [ViewDoc,DocPopoverPage]
})
export class UploadDocPageModule {}
