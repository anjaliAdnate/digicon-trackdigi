import { NavParams, ViewController } from "ionic-angular";
import { Component } from "@angular/core";
import { URLS } from "../../../../providers/urls";

@Component({
    selector: 'page-view-doc',
    templateUrl: './view-doc.html'
})
export class ViewDoc {
    islogin: any;
    _instData: any;
    imgUrl: any;
    constructor(
        public navparams: NavParams,
        public viewCtrl: ViewController,
        public urls: URLS
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("param1: ", this.navparams.get("param1"));
        this._instData = this.navparams.get("param1");
        var str = this._instData.imageURL;
        var str1 = str.split('public/');
        this.imgUrl = this.urls.mainURL + "/" + str1[1];
        console.log("img url: ", this._instData)
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}