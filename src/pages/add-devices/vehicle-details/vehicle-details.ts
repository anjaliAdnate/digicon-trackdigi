import { Component, OnInit, ViewChild } from "@angular/core";
import { IonicPage, NavParams, AlertController, NavController } from "ionic-angular";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { Chart } from 'chart.js';
import * as moment from 'moment';
import { URLS } from "../../../providers/urls";

@IonicPage()
@Component({
    selector: 'page-vehicle-details',
    templateUrl: './vehicle-details.html'
})

export class VehicleDetailsPage implements OnInit {

    @ViewChild('doughnutCanvas') doughnutCanvas;
    @ViewChild('barCanvas') barCanvas;
    @ViewChild('pieCanvas') pieCanvas;
    doughnutChart: any;
    barChart: any;
    pieChart: any;

    vehicleData: any = {};
    fromDate: string;
    toDate: string;
    islogin: any;
    curTime: any;
    data7Days: any = [];
    data7Data: any = [];
    lastStoppedAt: any;
    today_run: any;
    today_stop: any;

    constructor(
        public navParam: NavParams,
        public alertCtrl: AlertController,
        public apiCall: ApiServiceProvider,
        public navCtrl: NavController,
        public urls: URLS
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.vehicleData = navParam.get('param');
        console.log("vehicleData=> " + JSON.stringify(this.vehicleData))
        this.curTime = moment({ hours: 0 }).format();

        this.today_run = this.formatDurationHMS(this.vehicleData.today_running);
        this.today_stop = this.formatDurationHMS(this.vehicleData.today_stopped);

        var temp = new Date(this.vehicleData.lastStoppedAt);
        if (this.vehicleData.lastStoppedAt != null) {
            var fd = temp.getTime();
            var td = new Date().getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            this.lastStoppedAt = rhours + ' hrs ' + rminutes;
        } else {
            this.lastStoppedAt = '00' + ' hrs ' + '00';
        }
    }

    ngOnInit() {
        this.getChart();
        this.getChart2()
    }

    getChart2() {

        function dynamicColors() {
            return "#d80622";
        }

        function poolColors(a) {
            var pool = [];
            for (var i = 0; i < a; i++) {
                pool.push(dynamicColors());
            }
            return pool;
        }
        var _burl =this.urls.gpsURL + '/getDashGraph?imei=' + this.vehicleData.Device_ID + '&t=' + new Date(this.curTime).toISOString();
        this.apiCall.startLoading().present();
        this.apiCall.urlPassed(_burl)
            .subscribe(data => {
                this.apiCall.stopLoading();


                var days = [];
                days = data.map(function (d) {
                    var tempString = d._id['month'] + '/' + d._id['day'] + '/' + d._id['year'];
                    // return moment(new Date(tempString), 'DD/MM/YYYY').format('DD-MMM-YY');
                    return moment(new Date(tempString), 'DD/MM/YYYY').format('MMM-DD');
                })

                this.data7Days = days.map(function (d) {
                    // return new Date(d).toISOString();
                    return d;
                })
                var total = 0;

                for (var i = 0; i < data.length; i++) {
                    total += data[i].distance
                }
                var Data7 = [];
                Data7 = data.map(function (d) {
                    return (d.distance / total) * 100;
                })

                this.data7Data = Data7.map(function (d) {
                    return parseFloat(d).toFixed();
                });

                let that = this;

                if (that.barChart) {
                    that.barChart.destroy();
                }
                that.barChart = new Chart(that.barCanvas.nativeElement, {
                    type: 'bar',
                    data: {
                        labels: that.data7Days,
                        datasets: [{
                            data: that.data7Data,
                            backgroundColor: poolColors(that.data7Data.length),
                            borderColor: poolColors(that.data7Data.length),
                            borderWidth: 1
                        }]
                    },
                    options: {
                        "hover": {
                            "animationDuration": 0
                        },

                        "animation": {
                            "duration": 1,
                            "onComplete": function () {
                                var chartInstance = this.chart,
                                    ctx = chartInstance.ctx;

                                ctx.font = Chart.helpers.fontString(10, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'middle';

                                this.data.datasets.forEach(function (dataset, i) {
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function (bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                    });
                                });
                            }
                        },
                        legend: {
                            "display": false
                        },
                        tooltips: {
                            // enabled: false
                            position: 'nearest',
                            mode: 'index',
                            intersect: false,
                            yPadding: 5,
                            xPadding: 5,
                            caretSize: 8,
                            backgroundColor: 'rgba(72, 241, 12, 1)',
                            titleFontColor: 'black',
                            bodyFontColor: 'black',
                            borderColor: 'rgba(0,0,0,1)',
                            borderWidth: 2
                        },
                        plugins: {
                            labels: {
                                render: 'value',
                                fontSize: 0,
                                fontColor: '#fff',
                            }
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                                // type: 'time',
                                distribution: 'series',
                                ticks: {
                                    //this will fix your problem with NaN
                                    // callback: function (label, index, labels) {
                                    //     return label ? label : '';
                                    // }
                                    fontSize: 10,
                                },
                                gridLines: {
                                    display: false
                                },
                                // font: '10px',
                                barPercentage: 0.3,
                                categoryPercentage: 1
                            }],
                            yAxes: [{
                                display: false,
                                ticks: {
                                    beginAtZero: true,
                                    // callback: function (label, index, labels) {
                                    //     return label ? label : '';
                                    // }
                                },
                                gridLines: {
                                    display: true
                                },
                            }]
                        }
                    }
                })

                // that.barChart.bars.forceX();
            },
                err => {
                    console.log("error found=> ", err);
                    this.apiCall.stopLoading();
                })

    }

    getChart() {
        let that = this;
        Chart.pluginService.register({
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;

                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Arial';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#000';
                    var sidePadding = centerConfig.sidePadding || 20;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                    //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });

        // if the chart is not undefined (e.g. it has been created)
        // then destory the old one so we can create a new one later
        if (that.doughnutChart) {
            that.doughnutChart.destroy();
        }
        var totalCount = 0;
        totalCount = that.vehicleData.speedChart["0-20"] + that.vehicleData.speedChart["20-40"] + that.vehicleData.speedChart["40-60"] + that.vehicleData.speedChart["60-80"] + that.vehicleData.speedChart["80-100"] + that.vehicleData.speedChart[">100"];
        // console.log("totalCount=> " + totalCount)
        var val1, val2, val3, val4, val5, val6;
        val1 = (that.vehicleData.speedChart["0-20"] / totalCount) * 100;
        val2 = (that.vehicleData.speedChart["20-40"] / totalCount) * 100;
        val3 = (that.vehicleData.speedChart["40-60"] / totalCount) * 100;
        val4 = (that.vehicleData.speedChart["60-80"] / totalCount) * 100;
        val5 = (that.vehicleData.speedChart["80-100"] / totalCount) * 100;
        val6 = (that.vehicleData.speedChart[">100"] / totalCount) * 100;

        that.doughnutChart = new Chart(that.doughnutCanvas.nativeElement, {

            type: 'doughnut',
            data: {
                labels: ["0 to 20", "20 to 40", "40 to 60", "60 to 80", "80 to 100", "100+"],
                datasets: [{
                    label: 'My First dataset',
                    data: [parseFloat(val1).toFixed(2), parseFloat(val2).toFixed(2), parseFloat(val3).toFixed(), parseFloat(val4).toFixed(), parseFloat(val5).toFixed(), parseFloat(val6).toFixed()],
                    backgroundColor: [
                        '#e0e63c', '#00a1e4', '#b9a44c', '#f5b700', '#f45b69', '#d600d6'
                    ],
                    // borderColor: window.chartColors[colorName],
                    // borderWidth: 1,
                    // pointStyle: 'rectRot',
                    // pointRadius: 50,
                    // pointBorderColor: 'rgb(0, 0, 0)'
                }]
            },
            options: {
                plugins: {
                    labels: {
                        render: 'value',
                        fontSize: 10,
                        fontColor: '#fff',
                    }
                },
                // pieceLabel: {
                //     render: 'label',
                //     fontColor: ['white', 'white', 'white', 'white', 'white'],
                //     position: 'outside',
                //     segment: true
                // },
                elements: {
                    center: {
                        text: "Speed Data (%)",
                        color: '#d3d3d3', // Default is #000000
                        fontStyle: 'Roboto', // Default is Arial
                        fontSize: 0.1
                        // sidePadding: 20 // Defualt is 20 (as a percentage)
                    }
                },
                tooltips: {
                    // enabled: false
                    position: 'nearest',
                    mode: 'index',
                    intersect: false,
                    yPadding: 10,
                    xPadding: 10,
                    caretSize: 8,
                    backgroundColor: 'rgba(72, 241, 12, 1)',
                    titleFontColor: 'black',
                    bodyFontColor: 'black',
                    borderColor: 'rgba(0,0,0,1)',
                    borderWidth: 4
                },
                // pieceLabel: {
                //     mode: 'value',
                //     fontColor: ['white', 'white', 'white', 'white', 'white']
                // },
                responsive: true,
                legend: {
                    fontSize: 5,
                    fontFamily: "tamoha",
                    fontColor: "#fff",
                    position: "bottom",

                    labels: {
                        fontColor: 'rgb(255, 99, 132)',
                        fontSize: 7,
                        // boxWidth: 5,
                        usePointStyle: true
                    }
                },
            }
        });
    }
    showDaily() {
        this.navCtrl.push('DailyReportPage', {
            param: this.vehicleData
        })
    }

    showStoppage() {
        this.navCtrl.push('StoppagesRepoPage', {
            param: this.vehicleData
        })
    }

    showTrip() {
        this.navCtrl.push('TripReportPage', {
            param: this.vehicleData
        })
    }

    showNotif() {
        this.navCtrl.push('AllNotificationsPage', {
            param: this.vehicleData
        })
    }

    showHistory() {
        this.navCtrl.push('HistoryDevicePage', {
            device: this.vehicleData
        })
    }

    showLive() {
        this.navCtrl.push('LiveSingleDevice', {
            device: this.vehicleData
        })
    }

    parseDuration(duration) {
        let remain = duration

        let days = Math.floor(remain / (1000 * 60 * 60 * 24))
        remain = remain % (1000 * 60 * 60 * 24)

        let hours = Math.floor(remain / (1000 * 60 * 60))
        remain = remain % (1000 * 60 * 60)

        let minutes = Math.floor(remain / (1000 * 60))
        remain = remain % (1000 * 60)

        let seconds = Math.floor(remain / (1000))
        remain = remain % (1000)

        let milliseconds = remain

        return {
            days,
            hours,
            minutes,
            seconds,
            milliseconds
        };
    }

    formatTime(o, useMilli = false) {
        let parts = []
        if (o.days) {
            let ret = o.days + ' day'
            if (o.days !== 1) {
                ret += 's'
            }
            parts.push(ret)
        }
        if (o.hours) {
            let ret = o.hours + ' hour'
            if (o.hours !== 1) {
                ret += 's'
            }
            parts.push(ret)
        }
        if (o.minutes) {
            let ret = o.minutes + ' minute'
            if (o.minutes !== 1) {
                ret += 's'
            }
            parts.push(ret)

        }
        if (o.seconds) {
            let ret = o.seconds + ' second'
            if (o.seconds !== 1) {
                ret += 's'
            }
            parts.push(ret)
        }
        if (useMilli && o.milliseconds) {
            let ret = o.milliseconds + ' millisecond'
            if (o.milliseconds !== 1) {
                ret += 's'
            }
            parts.push(ret)
        }
        if (parts.length === 0) {
            return 'instantly'
        } else {
            return parts.join(' ')
        }
    }

    formatTimeHMS(o) {
        let hours = o.hours.toString()
        if (hours.length === 1) hours = '0' + hours

        let minutes = o.minutes.toString()
        if (minutes.length === 1) minutes = '0' + minutes

        let seconds = o.seconds.toString()
        if (seconds.length === 1) seconds = '0' + seconds

        // return hours + ":" + minutes + ":" + seconds
        return hours + " hrs " + minutes
    }

    formatDurationHMS(duration) {
        let time = this.parseDuration(duration)
        return this.formatTimeHMS(time)
    }

    formatDuration(duration, useMilli = false) {
        let time = this.parseDuration(duration)
        return this.formatTime(time, useMilli)
    }

}