import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WalletPage } from './wallet';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@NgModule({
  declarations: [
    WalletPage,
  ],
  imports: [
    IonicPageModule.forChild(WalletPage),
  ],
  providers: [
    InAppBrowser
  ]
})
export class WalletPageModule {}
