import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { URLS } from '../../../providers/urls';

@IonicPage()
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {
  enteredAmt: any;
  paytmregNum: any;
  paytmAmount: number;
  showbutton: boolean = false;
  hidebutton: any;
  orderID: string = "1fgddfg420ffvdffasf3ew";
  amounttobepaid: any;
  hidePaybutton: any;
  hideAddbutton: boolean;
  blockedAmt: any;


  constructor(
    public urls: URLS,
    public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, private iab: InAppBrowser, public apiCall: ApiServiceProvider, public toastCtrl: ToastController) {
    this.menu.enable(true);
  }


  toast = this.toastCtrl.create({
    duration: 5000,
    position: 'middle'
  });


  ionViewDidLoad() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    this.getPaytmBalance(userId);
    this.amounttobepaid = localStorage.getItem('tripFare') ? JSON.parse(localStorage.getItem('tripFare')) : 0;

    this.paytmregNum = localStorage.getItem('paytmregNum');
  }
  ionViewDidEnter() {

  }
  amount(cash) {
    console.log(cash);
    this.enteredAmt = cash;
  }

  changeNum() {
    this.navCtrl.push("PaytmwalletloginPage");
  }

  addMoney() {
    console.log("learning function=>", this.tempfunc);

    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    this.apiCall.startLoading();

    //  var userId = "5c1a28bc4e095c5648b3e6d0"
    var addAmountObj =
    {
      "CUST_ID": userId,
      "ORDER_ID": this.orderID,
      "TXN_AMOUNT": this.enteredAmt,
      "app_id": "OneQlikVTS"
    }
    var _baseURL = this.urls.mainURL + '/paytm/addMoney';
    this.apiCall.urlwithPayload(_baseURL, addAmountObj)
      .subscribe(res => {
        this.apiCall.stopLoading();
        console.log(res['_body']);
        var resTemplate = res['_body'];
        console.log(resTemplate);
        this.InappBrowserWindow(resTemplate);
      }, err => {
        this.apiCall.stopLoading();
        this.toast['message'] = "Server error on adding amount";
        this.toast.present()
      })
  }
  InappBrowserWindow(ress) {
    var outerThis = this;
    var pageContentUrl = 'data:text/html;base64,' + btoa(ress);
    //  console.log(ress._body)
    console.log("pagecontent url=> " + pageContentUrl);
    let browser = this.iab.create(pageContentUrl, '_self');

    // console.log("Opening Browser");
    // browser.executeScript({});

    browser.on('loadstop').subscribe(event => {
      console.log(event.url);
      if (event.url == this.urls.mainURL + '/paytm/callback') {
        console.log("callback");
        browser.close();
        outerThis.chacktransectionStatus();
      }

    });
  }

  chacktransectionStatus() {
    console.log("Amount Added");
    this.apiCall.startLoading();

    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    var _url = this.urls.mainURL + "/paytm/checkBalance?CUST_ID=" + userId + "&app_id=TrackDigi";

    this.apiCall.urlPassed(_url)
      .subscribe(res => {
        this.apiCall.stopLoading();
        var amountAfterCashAdded = res.response.amount;
        this.paytmAmount += amountAfterCashAdded;
        if (amountAfterCashAdded > this.paytmAmount) {

          this.toast.setMessage("Amount added successfuly");
          var pgchk = localStorage.getItem('flag');
          if (pgchk == "pushed before booking ride") {
            this.navCtrl.setRoot("LivePage");
          }
          this.toast.present();


        } else if (amountAfterCashAdded == this.paytmAmount) {
          console.log("Transection Failure,Please try again");
          // this.navCtrl.setRoot(LivePage);
          this.toast.setMessage("Transection failure ");
          this.toast.present();


        } else {
          if (pgchk == "pushed before booking ride") {
            this.navCtrl.setRoot("LivePage");
          }
          this.toast.setMessage("Transection failure or will work in production");
          this.toast.present();
        }
      }, err => {
        this.toast.setMessage("Server Error while checking balance");
        this.toast.present();
        this.apiCall.stopLoading();
      })

  }
  // ==================================================================================

  // method added after removing preauth==================================================
  releaseAMT() {
    this.navCtrl.push("PaymentSecurePage");
  }

  // ===========================================================================
  toasterror(errstatus) {
    console.log(errstatus);
    let toastCtrl1 = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom',
      cssClass: 'toastStyle'
    })

    toastCtrl1['message'] = errstatus;
    console.log(toastCtrl1);
    return toastCtrl1.present();


  }
  tempfunc = function () {
    var responseMsg = "Inside function";
    return responseMsg;
  }

  goBack() {
    this.navCtrl.pop();
  }


  getPaytmBalance(uId) {
    let toast_validation = this.toastCtrl.create({
      duration: 3000,
      position: 'middle'
    });
    var _ur = this.urls.mainURL + '/paytm/checkBalance?CUST_ID=' + uId + '&app_id=TrackDigi';
    this.apiCall.startLoading();
    this.apiCall.urlPassed(_ur)
      .subscribe(res => {
        this.apiCall.stopLoading();
        console.log("response");
        if (res.code == "ETIMEDOUT") {
          console.log("Server Error");
          toast_validation.setMessage("Etimeout server error");
          toast_validation.present();
        } else {
          console.log("response=>", res);
          this.paytmAmount = res.response.amount;

          var pgchk = localStorage.getItem('flag');
          if (pgchk == "pushed before booking ride") {
            if ((Number(this.paytmAmount) == 0) || (Number(this.amounttobepaid) < Number(this.paytmAmount))) {
              this.hidePaybutton = true;
            }
          }

        }
      })
  }

  getBlockedAmt() {
    var _burl = this.urls.mainURL + '/zogo/getInnitialBlockAmount';
    this.apiCall.startLoading();
    this.apiCall.urlPassed(_burl)
      .subscribe(res => {
        this.apiCall.stopLoading();
        console.log("billing amount=>", res['Block Amount']);
        this.blockedAmt = res['Block Amount'];
        if (this.paytmAmount < this.blockedAmt) {
          this.hideAddbutton = true;

        }
      }, err => {
        this.apiCall.stopLoading();
        console.log(err);
      })
  }


}


// http://localhost:3000/paytm/preAuth
// {
// "CUST_ID":"5bc86c5285303603e1046b27",
// "ORDER_ID":"465213434335345332434366523123647",
// "TXN_AMOUNT":"23.00",
// "DURATIONHRS":"70"
// }
