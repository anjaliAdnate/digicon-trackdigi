import { Component, OnInit } from "@angular/core";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { ViewController } from "ionic-angular";
import { URLS } from "../../../providers/urls";


@Component({
    selector: 'page-filter',
    templateUrl: './filter.html'
})
export class FilterPage implements OnInit {
    modal: any = {};
    filterList: any = [
        { filterID: 1, filterName: "Overspeed", filterValue: 'overspeed', checked: false },
        { filterID: 2, filterName: "Ignition", filterValue: 'IGN', checked: false },
        { filterID: 3, filterName: "Route", filterValue: 'Route', checked: false },
        { filterID: 4, filterName: "Geofence", filterValue: 'Geo-Fence', checked: false },
        { filterID: 5, filterName: "Stoppage", filterValue: 'MAXSTOPPAGE', checked: false },
        { filterID: 6, filterName: "Fuel", filterValue: 'Fuel', checked: false },
        { filterID: 7, filterName: "AC", filterValue: 'AC', checked: false },
        { filterID: 8, filterName: "Route POI", filterValue: 'route-poi', checked: false },
        { filterID: 9, filterName: "Power", filterValue: 'power', checked: false },
        { filterID: 10, filterName: "Immo", filterValue: 'immo', checked: false },
        { filterID: 11, filterName: "Theft", filterValue: 'theft', checked: false }
    ]
    selectedArray: any = [];
    islogin: any;
    vehicleList: any;
    checkedData: any;


    constructor(
        public apiCall: ApiServiceProvider,
        public viewCtrl: ViewController,
        public urls: URLS
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);

        if (localStorage.getItem("checkedList") != null) {
            this.checkedData = JSON.parse(localStorage.getItem("checkedList"));
            console.log("checked data=> " + this.checkedData)
        }
    }
    ngOnInit() {
        // this.getNotifTypes()
    }

    getNotifTypes() {
        this.apiCall.startLoading();
        this.apiCall.urlPassed(this.urls.mainURL + "/notifs/getTypes")
            .subscribe(data => {
                this.apiCall.stopLoading();
                console.log("notif type list: ", data);
                this.filterList = data;
            })
    }

    // dateOpen() {
    //     this.datePicker.show({
    //         date: new Date(),
    //         mode: 'date',
    //         androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    //     }).then(
    //         date => console.log('Got date: ', date),
    //         err => console.log('Error occurred while getting date: ', err)
    //     );
    // }

    // changeDTformat(fromDT) {
    //     let that = this;
    //     that.modal.fromDate = moment(new Date(fromDT), "YYYY-MM-DD").format('dd/mm/yyyy');
    //     console.log("moment=> ", that.modal.fromDate);
    // }


    applyFilter() {
        console.log(this.modal)
        console.log(this.selectedArray);
        localStorage.setItem("checkedList", JSON.stringify(this.selectedArray));

        if (this.selectedArray.length == 0) {
            localStorage.setItem("dates", "dates")
            this.viewCtrl.dismiss(this.modal);
        } else {
            localStorage.setItem("types", "types")
            this.viewCtrl.dismiss(this.selectedArray);
        }

    }
    close() {
        this.viewCtrl.dismiss();
    }

    selectMember(data) {
        console.log("selected=> " + JSON.stringify(data))

        if (data.checked == true) {
            this.selectedArray.push(data);
        } else {
            let newArray = this.selectedArray.filter(function (el) {
                return el.filterID !== data.filterID;
            });
            this.selectedArray = newArray;
        }

    }

    cancel() {
        console.log("do nothing");
        this.viewCtrl.dismiss();
    }

}