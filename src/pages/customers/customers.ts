import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Events, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { URLS } from '../../providers/urls';

@IonicPage()
@Component({
  selector: 'page-customers',
  templateUrl: 'customers.html',
})
export class CustomersPage implements OnInit {
  islogin: any;
  setsmsforotp: any;
  isSuperAdminStatus: any;
  customerslist: any;
  CustomerArray: any;
  CustomerArraySearch: any = [];

  CratedeOn: string;
  CustomerData: any;
  time: string;
  date: string;
  customer: any;
  customersignups: any;
  DeletedDevice: any;
  viewCtrl: any;
  isDealer: boolean;
  DeletedCustomer: any;
  expirydate: string;
  page: number = 1;
  limit: number = 5;
  ndata: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alerCtrl: AlertController,
    public events: Events,
    public urls: URLS
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("_id=> " + this.islogin._id);
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.setsmsforotp = localStorage.getItem('setsms');
    this.isSuperAdminStatus = this.islogin.isSuperAdmin;
    console.log("isSuperAdminStatus=> " + this.isSuperAdminStatus);
    this.isDealer = this.islogin.isDealer
    console.log("isDealer=> " + this.isDealer);
  }

  ngOnInit() {
    this.getcustomer();
  }

  dialNumber(number) {
    window.open('tel:' + number, '_system');
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getcustomer();
    refresher.complete();
  }

  getItems(ev: any) {
    console.log(ev.target.value, this.CustomerArray)
    const val = ev.target.value.trim();
    this.CustomerArraySearch = this.CustomerArray.filter((item) => {
      return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    console.log("search====", this.CustomerArraySearch);
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;

    setTimeout(() => {
      var baseURLp;
      baseURLp = this.urls.mainURL + '/users/getCust?uid=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit;
      that.ndata = [];
      this.apiCall.getCustomersCall(baseURLp)
        .subscribe(data => {
          that.ndata = data;

          for (let i = 0; i < that.ndata.length; i++) {
            that.CustomerData.push(that.ndata[i]);
          }

          that.CustomerArraySearch = that.CustomerData;
        },
          err => {
            this.apiCall.stopLoading();
            console.log("error found=> " + err);
          });

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  callSearch(ev) {
    console.log(ev.target.value)
    var searchKey = ev.target.value;
    this.page = 1;
    var bty = this.urls.usersURL + 'getCust?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit + '&search=' + searchKey;
    this.apiCall.urlPassed(bty)
      .subscribe(data => {
        this.CustomerArraySearch = data;
      },
        err => {
          var msg = JSON.parse(err._body);
          let toast = this.toastCtrl.create({
            message: msg.message,
            duration: 2000,
            position: 'bottom'
          });
          toast.present();
        })
  }

  onClear(ev) {
    // debugger;
    this.getcustomer();
    ev.target.value = '';
  }

  getcustomer() {
    console.log("getcustomer");
    var baseURLp = this.urls.mainURL + '/users/getCust?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
    this.apiCall.startLoading().present();
    this.apiCall.getCustomersCall(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.CustomerData = data;
        this.CustomerArraySearch = this.CustomerData;
      },
        err => {
          this.apiCall.stopLoading();
          var a = JSON.parse(err._body);
          var b = a.message;
          let toast = this.toastCtrl.create({
            message: b,
            duration: 2000,
            position: "bottom"
          })
          toast.present();
          toast.onDidDismiss(() => {
            // this.navCtrl.setRoot('DashboardPage');
          });
        });
  }

  CustomerStatus(Customersdeta) {
    var msg;
    if (Customersdeta.status) {
      msg = 'Do you want to Deactivate this customer?'
    } else {
      msg = 'Do you want to Activate this customer?'
    }
    let alert = this.alerCtrl.create({
      message: msg,
      buttons: [{
        text: 'YES',
        handler: () => {
          this.user_status(Customersdeta);
        }
      },
      {
        text: 'NO',
        handler: () => {
          this.getcustomer();
        }
      }]
    });
    alert.present();
  }

  user_status(Customersdeta) {
    var stat;
    if (Customersdeta.status) {
      stat = false;
    } else {
      stat = true;
    }

    var data = {
      "uId": Customersdeta._id,
      "loggedIn_id": this.islogin._id,
      "status": stat
    };
    this.apiCall.startLoading().present();
    this.apiCall.user_statusCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.DeletedDevice = data;
        console.log("DeletedDevice=> " + this.DeletedDevice)
        let toast = this.toastCtrl.create({
          message: 'Customer was updated successfully',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.getcustomer();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error => ", err)
          // var body = err._body;
          // var msg = JSON.parse(body);
          // let alert = this.alerCtrl.create({
          //   title: 'Oops!',
          //   message: msg.message,
          //   buttons: ['OK']
          // });
          // alert.present();
        });
  }

  openupdateCustomersModal(Customersdetails) {
    console.log('Opening Modal openAdddeviceModal');
    this.customer = Customersdetails;
    // console.log("search array data=> " + this.CustomerArraySearch)
    let modal = this.modalCtrl.create('UpdateCustModalPage', {
      param: this.customer
    });
    modal.onDidDismiss((data) => {
      console.log(data)
      // this.getcustomer();
      this.navCtrl.setRoot("CustomersPage")
    });
    modal.present();
  }

  openAdddeviceModal(Customersdetails) {
    console.log('Opening Modal openAdddeviceModal');
    this.customer = Customersdetails;
    console.log("customers=> ", this.customer)
    console.log(this.customer._id)
    console.log(this.customer.email)

    let profileModal = this.modalCtrl.create('AddDeviceModalPage', { custDet: this.customer });
    profileModal.onDidDismiss(data => {
      console.log("vehData=> " + JSON.stringify(data));
      this.getcustomer();
    });
    profileModal.present();

  }
  dismiss() {
    this.viewCtrl.dismiss();
  }


  openAddCustomerModal() {
    let modal = this.modalCtrl.create('AddCustomerModal');
    modal.onDidDismiss(() => {
      console.log("modal dismissed!")
      this.getcustomer();
    })
    modal.present();
  }

  switchUser(cust_id) {
    //debugger;
    console.log(cust_id)
    localStorage.setItem('isDealervalue', 'true');
    // $rootScope.dealer = $rootScope.islogin;
    localStorage.setItem('dealer', JSON.stringify(this.islogin));

    localStorage.setItem('custumer_status', 'ON');
    localStorage.setItem('dealer_status', 'OFF');

    this.apiCall.getcustToken(cust_id)
      .subscribe(res => {
        console.log('UserChangeObj=>', res)
        var custToken = res;

        var logindata = JSON.stringify(custToken);
        var logindetails = JSON.parse(logindata);
        var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
        // console.log('token=>', logindata);

        var details = JSON.parse(userDetails);
        // console.log(details.isDealer);
        localStorage.setItem("loginflag", "loginflag");
        localStorage.setItem('details', JSON.stringify(details));

        var dealerSwitchObj = {
          "logindata": logindata,
          "details": userDetails,
          'condition_chk': details.isDealer
        }

        var temp = localStorage.getItem('isDealervalue');
        console.log("temp=> ", temp);
        this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
        this.events.publish("sidemenu:event", temp);
        this.navCtrl.setRoot('DashboardPage');

      }, err => {
        console.log(err);
      })
  }

  DelateCustomer(_id) {
    let alert = this.alerCtrl.create({
      message: 'Do you want to delete this Customer?',
      buttons: [{
        text: 'No'
      },
      {
        text: 'YES',
        handler: () => {
          this.deleteCus(_id);
        }
      }]
    });
    alert.present();
  }


  deleteCus(_id) {
    var data = {
      "userId": _id,
      'deleteuser': true
    }
    var burl = this.urls.usersURL + 'deleteUser';
    this.apiCall.urlwithPayload(burl, data).
      subscribe(data => {
        this.DeletedCustomer = data;
        let toast = this.toastCtrl.create({
          message: 'Deleted customer successfully.',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.getcustomer();
        });

        toast.present();
      },
        err => {
          console.log(err);
          // this.apiCall.stopLoading();
          // var body = err._body;
          // var msg = JSON.parse(body);
          // let alert = this.alerCtrl.create({
          //   title: 'Oops!',
          //   message: msg.message,
          //   buttons: ['OK']
          // });
          // alert.present();
        });
  }


}
