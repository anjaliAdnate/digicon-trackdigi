import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCustomerModal } from './add-customer-modal';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
// import { FileTransfer, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';

@NgModule({
  declarations: [
    AddCustomerModal
  ],
  imports: [
    IonicPageModule.forChild(AddCustomerModal)
  ],
  providers: [
    Camera,
    File,
    FilePath,
    FileTransfer,
    FileTransferObject,
  ]
})
export class AddCustomerModalModule { }