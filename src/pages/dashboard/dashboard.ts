import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { LivePage } from '../live/live';
import { AddDevicesPage } from '../add-devices/add-devices';
import { HistoryDevicePage } from '../history-device/history-device';
import { GeofencePage } from '../geofence/geofence';
import { AllNotificationsPage } from '../all-notifications/all-notifications';
import '../../assets/js/chartjs-plugin-labels.min.js';
import * as io from 'socket.io-client';   // with ES6 import
import { PushOptions, PushObject, Push } from '@ionic-native/push';
import { URLS } from '../../providers/urls';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage implements OnInit {
  public cartCount: number = 0;

  islogin: any;
  uuid: string;
  PushToken: string;
  dealerStatus: any;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;
  from: any;
  to: any;
  TotalDevice: any[];
  devices: any;
  Running: any;
  IDLING: any;
  Stopped: any;
  Maintance: any;
  OutOffReach: any;
  NogpsDevice: any;
  totalVechiles: any;
  devices1243: any[];
  isdevice: string;
  socket: any;
  token: string;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public push: Push,
    public toastCtrl: ToastController,
    public urls: URLS) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    ///////////////////////////////
    this.events.publish('user:updated', this.islogin);
    /////////////////////////////////


    this.token = localStorage.getItem("DEVICE_TOKEN");
    console.log("device token dashboard=> ", this.token)
    // console.log("islogin=> " + JSON.stringify(this.islogin));
    this.uuid = localStorage.getItem('uuid');
    // console.log("uuid=> " + this.uuid);
    this.PushToken = localStorage.getItem('PushToken');
    // console.log("pushtoken=> " + this.PushToken);
    this.dealerStatus = this.islogin.isDealer;
    // console.log("dealer status => " + this.dealerStatus);

    this.to = new Date().toISOString();
    var d = new Date();
    var a = d.setHours(0, 0, 0, 0)
    this.from = new Date(a).toISOString();
    // console.log("form=> " + this.from);

    ///////////////=======Push notifications========////////////////
    this.events.subscribe('cart:updated', (count) => {
      this.cartCount = count;
    });

    this.socket = io(this.urls.mainURL + '/notifIO', {
      transports: ['websocket', 'polling']
    });

    this.socket.on('connect', () => {
      console.log('IO Connected tabs');
      console.log("socket connected tabs", this.socket.connected)
    });

    this.socket.on(this.islogin._id, (msg) => {
      this.cartCount += 1;
    })
    ///////////////=======Push notifications========////////////////
  }

  ngOnInit() {
    this.getDashboarddevices();

    localStorage.removeItem("LiveDevice");
  }

  doRefresh(refresher) {
    this.getDashboarddevices();
    refresher.complete();
  }

  seeNotifications() {
    this.navCtrl.push(AllNotificationsPage);
  }

  getChart() {
    let that = this;
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          var ctx = chart.chart.ctx;

          //Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = centerConfig.color || '#000';
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });

    if (this.doughnutChart) {
      this.doughnutChart.destroy();
    }
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

      type: 'doughnut',
      data: {
        labels: ["RUNNING", "IDLING", "STOPPED", "MAINTENANCE", "OUT OF REACH", "NO GPS"],
        datasets: [{
          label: '# of Votes',
          data: this.TotalDevice,
          backgroundColor: ['green', '#FCEF07', 'red', '#d27d7d', 'blue', 'gray'],
        }]
      },
      options: {
        plugins: {
          labels: {
            render: 'value',
            fontSize: 16,
            fontColor: '#fff',
            images: [
              {
                src: 'image.png',
                width: 16,
                height: 16
              }
            ],
            outsidePadding: 4,
            textMargin: 4
          }
        },
        elements: {
          center: {
            text: this.totalVechiles,
            color: '#d80622', // Default is #000000
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20 // Defualt is 20 (as a percentage)
          }
        },
        tooltips: {
          enabled: false
        },
        pieceLabel: {
          mode: 'value',
          fontColor: ['white', 'white', 'white', 'white', 'white']
        },
        responsive: true,
        legend: {
          display: true,
          labels: {
            fontColor: 'rgb(255, 99, 132)',
            fontSize: 8
          },
          position: 'bottom',
        },
        onClick: function (event, elementsAtEvent) {
          var activePoints = elementsAtEvent;
          if (activePoints[0]) {
            var chartData = activePoints[0]['_chart'].config.data;
            var idx = activePoints[0]['_index'];
            var label = chartData.labels[idx];
            var value = chartData.datasets[0].data[idx];
            localStorage.setItem('status', label);
            that.navCtrl.push(AddDevicesPage, {
              label: label,
              value: value
            });
          }
        }
      }
    });

  }

  historyDevice() {
    localStorage.setItem("MainHistory", "MainHistory");
    this.navCtrl.push(HistoryDevicePage);
  }

  opennotify() {
    this.navCtrl.push(AllNotificationsPage)
  }

  addPushNotify() {
    let that = this;
    that.token = localStorage.getItem("DEVICE_TOKEN");
    if (that.token != null) {
      var pushdata = {
        "uid": that.islogin._id,
        "token": that.token,
        "os": "android"
      }
      that.apiCall.pushnotifyCall(pushdata)
        .subscribe(data => {
          console.log("push notifications updated " + data.message)
        },
          error => {
            console.log(error);
          });
    } else {
      this.pushSetup()
    }
  }

  pushSetup() {
    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '14752485561',
        // 14752485561
        // icon: './assets/imgs/yellow-bus-icon-40028.png'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      if (notification.additionalData.foreground) {
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        });
        toast.present();
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("device token => " + registration.registrationId);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId)
        that.addPushNotify();
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
      // alert('Error with Push plugin' + error)
    });
  }


  getDashboarddevices() {
    console.log("getdevices");

    var _baseUrl = this.urls.mainURL + '/gps/getDashboard?email=' + this.islogin.email + '&from=' + this.from + '&to=' + this.to + '&id=' + this.islogin._id;

    if (this.islogin.isSuperAdmin == true) {
      _baseUrl += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        _baseUrl += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlPassed(_baseUrl)
      .subscribe(data => {
        this.apiCall.stopLoading();

        ///// add push token to server //////////////
        this.addPushNotify();
        /////////////////////////////////////////////

        this.TotalDevice = [];
        this.devices = data;
        // console.log("devices => " + JSON.stringify(this.devices));

        this.Running = this.devices.running_devices;
        this.TotalDevice.push(this.Running)
        this.IDLING = this.devices["Ideal Devices"]
        this.TotalDevice.push(this.IDLING);
        this.Stopped = this.devices["OFF Devices"]
        this.TotalDevice.push(this.Stopped);
        this.Maintance = this.devices["Maintance Device"]
        this.TotalDevice.push(this.Maintance);
        this.OutOffReach = this.devices["OutOfReach"]
        this.TotalDevice.push(this.OutOffReach);
        this.NogpsDevice = this.devices["no_gps_fix_devices"]
        this.TotalDevice.push(this.NogpsDevice);
        // console.log("total devices => " + this.TotalDevice);
        this.totalVechiles = this.Running + this.IDLING + this.Stopped + this.Maintance + this.OutOffReach + this.NogpsDevice;
        // console.log(this.devices.off_ids);
        this.getChart();

      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getStopdevices() {
    console.log("getStopdevices");
    this.apiCall.startLoading().present();
    this.apiCall.stoppedDevices(this.islogin._id, this.islogin.email, this.devices.off_ids)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.devices = data;
        // console.log("devices 1 => " + this.devices);
        this.devices1243 = [];
        this.devices = data.devices.reverse();
        this.devices1243.push(data);
        // console.log("devices1243=> " + this.devices1243);

        this.devices = this.devices;
        // console.log(this.devices);
        localStorage.setItem('devices', this.devices);
        this.isdevice = localStorage.getItem('devices');

        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
        console.log(this.islogin);
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  vehiclelist() {
    console.log("coming soon");
    this.navCtrl.push(AddDevicesPage);
  }

  live() {
    this.navCtrl.push(LivePage);
  }

  geofence() {
    this.navCtrl.push(GeofencePage);
  }

}
