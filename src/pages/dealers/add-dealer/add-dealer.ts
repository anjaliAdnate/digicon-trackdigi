import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-add-customer-model',
  templateUrl: 'add-dealer.html',
})
export class AddDealerPage {
  addDealerform: FormGroup;
  islogin: any;
  submitAttempt: boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public apicall: ApiServiceProvider,
    public alerCtrl: AlertController,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,

  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));

    this.addDealerform = formBuilder.group({
      userId: ['', Validators.required],
      Firstname: ['', Validators.required],
      LastName: ['', Validators.required],
      emailid: ['', Validators.email],
      contact_num: [''],
      password: ['', Validators.required],
      cpassword: ['', Validators.required],
      address: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddDealerPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  _submit() {
    this.submitAttempt = true;
    if (this.addDealerform.valid) {

      // if (this.islogin.role == 'supAdm') {  

      // only superadmin can add the dealer, thats why we dont need to check condition if it is superadmin or dealer
      var payload = {
        address: this.addDealerform.value.address,
        custumer: false,
        email: this.addDealerform.value.emailid,
        first_name: this.addDealerform.value.Firstname,
        // imageDoc: [{ doctype: "", image: "" }],
        isDealer: this.islogin.isDealer,
        last_name: this.addDealerform.value.LastName,
        org_name: this.islogin._orgName,
        password: this.addDealerform.value.password,
        supAdmin: this.islogin._id,
        sysadmin: this.islogin.isSuperAdmin,
        user_id: this.addDealerform.value.userId
      }

      this.apicall.startLoading().present();
      this.apicall.signupApi(payload)
        .subscribe(data => {
          this.apicall.stopLoading();
          console.log("dealer added data=> ", data)
          let toast = this.toastCtrl.create({
            message: 'Dealer added successfully!',
            position: 'top',
            duration: 1500
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
            this.viewCtrl.dismiss();
          });

          toast.present();
        },
          err => {
            this.apicall.stopLoading();
            console.log("error occured=> ", err)
            var body = err._body;
            var msg = JSON.parse(body);
            var namepass = [];
            namepass = msg.split(":");
            var name = namepass[1];
            let alert = this.alerCtrl.create({
              // title: 'Oops!',
              message: name,
              buttons: ['OK']
            });
            alert.present();
          });

    }
  }

}
