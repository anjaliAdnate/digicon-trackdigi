import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { AlertController, ToastController, NavController, IonicPage, ViewController, Events } from "ionic-angular";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GoogleMaps, GoogleMap, GoogleMapsEvent, CameraPosition, Marker, LatLng, ILatLng, Polygon } from '@ionic-native/google-maps';
import { NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { URLS } from "../../../providers/urls";
declare var google;

@IonicPage()
@Component({
    selector: 'page-add-geofence',
    templateUrl: './add-geofence.html',
})
export class AddGeofencePage implements OnInit {
    map: GoogleMap;
    mapElement: HTMLElement;
    geofenceForm: FormGroup;
    submitAttempt: boolean;
    geofencedetails: any;
    finalcordinate: any = [];
    islogin: any;
    devicesadd: any;
    curLat: any; curLng: any;
    entering: any;
    exiting: any;
    cord: any = [];
    isdevice: string;
    markers: any;
    acService: any;
    autocompleteItems: any = [];
    autocomplete: any = {};
    newLat: any;
    newLng: any;
    storedLatLng: any = [];

    constructor(
        public apiCall: ApiServiceProvider,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public fb: FormBuilder,
        public navCtrl: NavController, private googleMaps: GoogleMaps, public geoLocation: Geolocation,
        public viewCtrl: ViewController,
        private nativeGeocoder: NativeGeocoder,
        public events: Events,
        public urls: URLS
    ) {

        this.islogin = JSON.parse(localStorage.getItem('details')) || {};

        this.geofenceForm = fb.group({
            geofence_name: ['', Validators.required],
            check: [false],
            check1: [false]
        });

        this.acService = new google.maps.places.AutocompleteService();
    }

    ngOnInit() {
        // this.runGeofenceMaps();
        this.drawGeofence();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    enteringFunc() {
        console.log(this.geofenceForm.value.check);
        this.entering = this.geofenceForm.value.check;
    }

    exitingFunc() {
        console.log(this.geofenceForm.value.check1);
        this.exiting = this.geofenceForm.value.check1;
    }

    creategoefence() {
        let that = this;
        that.submitAttempt = true;

        for (var r = 0; r < that.storedLatLng.length; r++) {
            var a = [];
            a.push(parseFloat(that.storedLatLng[r].lng));
            a.push(parseFloat(that.storedLatLng[r].lat));
            that.cord.push(a);
        }

        that.finalcordinate.push(that.cord);
        if (this.geofenceForm.valid) {
            if (this.entering || this.exiting) {
                // if (this.geofenceForm.value.geofence_name && this.finalcordinate.length) {
                if (this.geofenceForm.value.geofence_name && that.finalcordinate.length) {

                    var data = {
                        "uid": this.islogin._id,
                        "geoname": this.geofenceForm.value.geofence_name,
                        "entering": this.entering,
                        "exiting": this.exiting,
                        // "geofence": this.finalcordinate
                        "geofence": that.finalcordinate
                    }
                    this.apiCall.startLoading().present();
                    this.apiCall.addgeofenceCall(data)
                        .subscribe(data => {
                            this.apiCall.stopLoading();
                            this.devicesadd = data;
                            console.log(this.devicesadd);

                            let toast = this.toastCtrl.create({
                                message: 'Created geofence successfully',
                                position: 'bottom',
                                duration: 2000
                            });

                            toast.onDidDismiss(() => {
                                console.log('Dismissed toast');
                                // this.navCtrl.push(GeofencePage);
                                // this.viewCtrl.dismiss();
                                this.events.publish('reloadDetails');
                                this.navCtrl.pop()
                            });

                            toast.present();
                        }, err => {
                            this.apiCall.stopLoading();
                            let alert = this.alertCtrl.create({
                                message: 'Please draw valid geofence..',
                                buttons: [{
                                    text: 'OK', handler: () => {
                                        that.storedLatLng = [];
                                        that.finalcordinate = [];
                                        that.cord = [];
                                        this.drawGeofence();
                                    }
                                }]
                            });
                            alert.present();
                            console.log(err);
                        });
                } else {

                    let toast = this.toastCtrl.create({
                        message: 'Select Geofence On Map!',
                        position: 'top',
                        duration: 2000
                    });

                    toast.present();
                }
            } else {
                let alert = this.alertCtrl.create({
                    message: 'All fields are required!',
                    buttons: ['Try Again']
                });
                alert.present();
            }
        }
    }

    updateSearch() {
        // debugger
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        let that = this;
        let config = {
            //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
            input: that.autocomplete.query,
            componentRestrictions: {}
        }
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            console.log("lat long not find ", predictions);

            that.autocompleteItems = [];
            predictions.forEach(function (prediction) {
                that.autocompleteItems.push(prediction);
            });
            console.log("autocompleteItems=> " + that.autocompleteItems)
        });
    }

    chooseItem(item) {
        let that = this;
        that.autocomplete.query = item.description;
        console.log("console items=> " + JSON.stringify(item))
        that.autocompleteItems = [];

        let options: NativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
        };

        this.nativeGeocoder.forwardGeocode(item.description, options)
            .then((coordinates: NativeGeocoderForwardResult[]) => {
                console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude)
                that.newLat = coordinates[0].latitude;
                that.newLng = coordinates[0].longitude;
                let pos: CameraPosition<ILatLng> = {
                    target: new LatLng(that.newLat, that.newLng),
                    zoom: 15,
                    tilt: 30
                };
                this.map.moveCamera(pos);
                this.map.addMarker({
                    title: '',
                    position: new LatLng(that.newLat, that.newLng),
                }).then((data) => {
                    console.log("Marker added")
                })

            })
            .catch((error: any) => console.log(error));

    }

    drawGeofence() {

        if (this.map != undefined) {
            this.map.remove();
        }

        this.mapElement = document.getElementById('mapGeofence');
        console.log(this.mapElement);

        this.map = this.googleMaps.create(this.mapElement);

        // Wait the MAP_READY before using any methods.
        this.map.one(GoogleMapsEvent.MAP_READY)
            .then(() => {
                console.log('Map is ready!');

                this.geoLocation.getCurrentPosition().then((resp) => {

                    let pos: CameraPosition<ILatLng> = {
                        target: new LatLng(resp.coords.latitude, resp.coords.longitude),
                        zoom: 18,
                        tilt: 30
                    };
                    this.map.moveCamera(pos);
                    this.map.addMarker({
                        title: '',
                        position: new LatLng(resp.coords.latitude, resp.coords.longitude),
                    }).then((data) => {
                        console.log("Marker added")
                    })
                });
                let that = this;
                that.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
                    (data) => {
                        that.storedLatLng.push(data[0]);

                        that.map.addMarker({
                            position: data[0],
                            icon: './assets/imgs/circle1.png'
                        }).then((mark: Marker) => {
                            console.log("Marker added")
                            mark.on(GoogleMapsEvent.MARKER_CLICK).subscribe((latLng: LatLng) => {

                                that.storedLatLng.push(that.storedLatLng[0])  // store first lat lng at last also

                                let GORYOKAKU_POINTS: ILatLng[] = that.storedLatLng;

                                that.map.addPolygon({
                                    'points': GORYOKAKU_POINTS,
                                    'strokeColor': '#000',
                                    'fillColor': '#ffff00',
                                    'strokeWidth': 5
                                }).then((polygon: Polygon) => {
                                    console.log("GORYOKAKU_POINTS=> " + JSON.stringify(GORYOKAKU_POINTS));
                                    // this.disableTap()
                                });
                            });

                        });
                        console.log(JSON.stringify(that.storedLatLng));

                        if (that.storedLatLng.length > 1) {
                            that.map.addPolyline({
                                points: that.storedLatLng,
                                color: '#000000',
                                width: 3,
                                geodesic: true
                            })
                        }
                    }
                );
            });
    }

}