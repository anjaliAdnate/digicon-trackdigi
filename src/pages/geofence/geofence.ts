import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Navbar, ModalController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { URLS } from '../../providers/urls';

@IonicPage()
@Component({
  selector: 'page-geofence',
  templateUrl: 'geofence.html',
})
export class GeofencePage implements OnInit {
  @ViewChild(Navbar) navBar: Navbar;
  islogin: any;
  setsmsforotp: string;
  isdevice: string;
  devices: any;
  devices1243: any[];
  DeletedDevice: any;
  statusofgeofence: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public alerCtrl: AlertController,
    public modalCtrl: ModalController,
    public events: Events,
    public urls: URLS) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    
    this.setsmsforotp = localStorage.getItem('setsms');
    this.isdevice = localStorage.getItem('cordinates');
    this.events.subscribe('reloadDetails', () => {
      //call methods to refresh content
      var ur = this.urls.geofencingURL + '/getallgeofence?uid=' + this.islogin._id;
      this.apiCall.urlPassed(ur)
        .subscribe(data => {
          this.devices1243 = [];
          this.devices = data;
          console.log("devices=> ", this.devices);
          localStorage.setItem('devices', this.devices);
          this.isdevice = localStorage.getItem('devices');
          console.log("isdevices=> ", this.isdevice);
        },
          err => {
            console.log("error => ", err);
          });
    });
  }

  ngOnInit() {
    if (localStorage.getItem("SCREEN") != null) {
      this.navBar.backButtonClick = (e: UIEvent) => {
        // todo something
        // this.navController.pop();
        console.log("back button poped")
        if (localStorage.getItem("SCREEN") != null) {
          if (localStorage.getItem("SCREEN") === 'live') {
            this.navCtrl.setRoot('LivePage');
          } else {
            if (localStorage.getItem("SCREEN") === 'dashboard') {
              this.navCtrl.setRoot('DashboardPage');
            }
          }
        }
      }
    }
    this.getgeofence();
  }

  addgeofence() {
    this.navCtrl.push('AddGeofencePage');
  }


  getgeofence() {
    console.log("getgeofence shape");
    var baseURLp = this.urls.mainURL + '/geofencing/getallgeofence?uid=' + this.islogin._id;

    this.apiCall.startLoading().present();
    this.apiCall.urlPassed(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        console.log("devices=> ", this.devices);
        localStorage.setItem('devices', this.devices);
        this.isdevice = localStorage.getItem('devices');
        console.log("isdevices=> ", this.isdevice);
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error => ", err);
        });
  }

  deleteGeo(_id) {
    var burl = this.urls.geofencingURL + '/deletegeofence?id=' + _id;
    this.apiCall.urlPassed(burl).
      subscribe(data => {
        // this.apiCall.stopLoading();
        this.DeletedDevice = data;
        let toast = this.toastCtrl.create({
          message: 'Deleted Geofence Area successfully.',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.getgeofence();
        });

        toast.present();
      },
        err => {
          // this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alerCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }

  DelateGeofence(_id) {
    let alert = this.alerCtrl.create({
      message: 'Do you want to delete this geofence area?',
      buttons: [{
        text: 'No'
      },
      {
        text: 'YES',
        handler: () => {
          this.deleteGeo(_id);
        }
      }]
    });
    alert.present();
  }

  DisplayDataOnMap(item) {
    var baseURLp = this.urls.mainURL + '/geofencing/geofencestatus?gid=' + item._id + '&status=' + item.status + '&entering=' + item.entering + '&exiting=' + item.exiting;
    this.apiCall.urlPassed(baseURLp)
      .subscribe(data => {
        this.statusofgeofence = data;
      },
        err => {
          console.log(err);
        });
  }

  geofenceShow(item) {
    this.navCtrl.push('GeofenceShowPage', {
      param: item
    });
  }
}
