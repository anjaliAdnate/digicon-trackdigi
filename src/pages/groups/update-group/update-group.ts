import { Component, OnInit } from "@angular/core";
import { NavController, ViewController } from "ionic-angular";
import { NavParams } from "ionic-angular";
import { AlertController } from "ionic-angular";
import { ModalController } from "ionic-angular";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";

@Component({
    selector: 'page-update-model',
    templateUrl: './update-group.html'
})
export class UpdateGroup implements OnInit {

    islogin: any;
    GroupStatus: { name: string; }[];
    groupForm: FormGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams, public apigroupupdatecall: ApiServiceProvider,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController,
        public formBuilder: FormBuilder,
        public viewCtrl:ViewController) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        
        console.log("_id=> " + this.islogin._id); this.GroupStatus = [
            {
                name: "Active"
            },
            {
                name: "InActive"
            }];


            this.groupForm = formBuilder.group({
                group_name: ['', Validators.required],
                status: ['', Validators.required],
                grouptype: ['']
            })

    }

    ngOnInit() {

    }


    dismiss() {
        this.viewCtrl.dismiss();
    }
}