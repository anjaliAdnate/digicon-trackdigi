import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { URLS } from '../../providers/urls';

@IonicPage()
@Component({
  selector: 'page-over-speed-repo',
  templateUrl: 'over-speed-repo.html',
})
export class OverSpeedRepoPage implements OnInit {


  overSpeeddevice_id: any;
  overspeedReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any;
  isdevice: string;
  datetime: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public urls: URLS,
    public apicalloverspeed: ApiServiceProvider, public alertCtrl: AlertController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
  }

  ngOnInit() {
    this.getdevices();
  }

  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }

  getOverspeeddevice(selectedVehicle) {
    // console.log("selectedVehicle=> ", selectedVehicle)
    this.overSpeeddevice_id = selectedVehicle._id;
    // console.log("selected vehicle=> " + this.overSpeeddevice_id)
  }

  getdevices() {
    var baseURLp = this.urls.mainURL + '/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalloverspeed.startLoading().present();
    // this.apicalloverspeed.livedatacall(this.islogin._id, this.islogin.email)
    this.apicalloverspeed.urlPassed(baseURLp)
      .subscribe(data => {
        this.apicalloverspeed.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.isdevice = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicalloverspeed.stopLoading();
          console.log(err);
        });
  }


  getOverSpeedReport(starttime, endtime) {
    var baseUrl;
    if (this.overSpeeddevice_id == undefined) {
      baseUrl = this.urls.mainURL + "/notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id

    } else {
      baseUrl = this.urls.mainURL + "/notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id + '&device=' + this.overSpeeddevice_id
    }


    this.apicalloverspeed.startLoading().present();

    this.apicalloverspeed.urlPassed(baseUrl)
      .subscribe(data => {
        this.apicalloverspeed.stopLoading();
        this.overspeedReport = data;
        console.log(this.overspeedReport);
        if (this.overspeedReport.length == 0) {
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }

      }, error => {
        this.apicalloverspeed.stopLoading();
        console.log(error);
      })
  }

}
