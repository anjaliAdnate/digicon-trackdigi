import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController, Events, AlertController } from 'ionic-angular';
import { GoogleMaps, GoogleMap, CameraPosition, Marker, LatLng, ILatLng, GoogleMapsAnimation, MyLocation, GoogleMapsEvent } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoderOptions, NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { URLS } from '../../../providers/urls';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-add-poi',
  templateUrl: 'add-poi.html',
})
export class AddPoiPage implements OnInit {
  map: GoogleMap;
  submitAttempt: boolean;
  autocomplete: any = {};
  autocompleteItems: any = [];
  acService: any;
  newLat: any;
  newLng: any;
  islogin: any;
  poi_name: any;
  radius: any;
  markerObj: any;
  portstemp: any[] = [];
  mappedveh: any;
  navParameters: any;
  text: string;
  changedEvent: any;
  arrOBJ: any[] = [];
  sortedArr: any[] = [];
  beforSortArr: any[] = [];

  constructor(
    public urls: URLS,
    public navParam: NavParams, public alertCtrl: AlertController, public events: Events, private nativeGeocoder: NativeGeocoder, public geoLocation: Geolocation, public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public apicall: ApiServiceProvider) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    if (this.navParam.get("param") != null) {
      this.text = "Edit";
      this.navParameters = this.navParam.get("param");
      this.radius = this.navParameters.radius;
      this.poi_name = this.navParameters.poi.poiname;
      this.getdevices();
      console.log("navparams: ", JSON.stringify(this.navParam.get("param")))
    } else {
      this.text = "Add";
    }
    this.acService = new google.maps.places.AutocompleteService();
  }

  ngOnInit() {
    this.loadMap();
  }

  onchange(veh) {
    console.log("onchange: ", veh)
    console.log("selected multiple value: ", this.mappedveh)
  }

  contains = function (needle) {
    // Per spec, the way to identify NaN is that it is not equal to itself
    var findNaN = needle !== needle;
    var indexOf;

    if (!findNaN && typeof Array.prototype.indexOf === 'function') {
      indexOf = Array.prototype.indexOf;
    } else {
      indexOf = function (needle) {
        var i = -1, index = -1;

        for (i = 0; i < this.length; i++) {
          var item = this[i];

          if ((findNaN && item !== item) || item === needle) {
            index = i;
            break;
          }
        }

        return index;
      };
    }

    return indexOf.call(this, needle) > -1;
  };

  ionViewDidLoad() { }

  loadMap() {
    // if (this.map != undefined) {
    //   this.map.clear();
    // }
    this.map = GoogleMaps.create('map_canvas');
    if (this.navParam.get("param") == null) {
      this.onButtonClick()
    } else {
      this.callthis();
    }

  }

  callthis() {
    this.map.clear();
    // Move the map camera to the location with animation
    this.map.animateCamera({
      target: {
        "lat": this.navParameters.poi.location.coordinates[1],
        "lng": this.navParameters.poi.location.coordinates[0]
      },
      zoom: 17,
      tilt: 30
    })
      .then(() => {
        this.map.addMarker({
          title: 'Add POI here(Drag Me)',
          // snippet: 'This plugin is awesome!',
          position: {
            "lat": this.navParameters.poi.location.coordinates[1],
            "lng": this.navParameters.poi.location.coordinates[0]
          },
          animation: GoogleMapsAnimation.BOUNCE,
          draggable: true
        }).then((marker: Marker) => {
          this.markerObj = marker;
          marker.on(GoogleMapsEvent.MARKER_DRAG_END)
            .subscribe(() => {
              this.markerObj = undefined;
              // this.markerObj = marker.getPosition();
              this.markerObj = marker;
            });
        })
      });

  }

  onButtonClick() {
    this.map.clear();

    // Get the location of you
    this.map.getMyLocation()
      .then((location: MyLocation) => {
        console.log(JSON.stringify(location, null, 2));

        // Move the map camera to the location with animation
        this.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          tilt: 30
        })
          .then(() => {
            this.map.addMarker({
              title: 'Add POI here(Drag Me)',
              position: location.latLng,
              animation: GoogleMapsAnimation.BOUNCE,
              draggable: true
            }).then((marker: Marker) => {
              this.markerObj = marker;
              marker.on(GoogleMapsEvent.MARKER_DRAG_END)
                .subscribe(() => {
                  this.markerObj = undefined;
                  this.markerObj = marker;
                });
            })
          });
      });
  }

  back() {
    this.navCtrl.pop();
  }

  updateSearch() {
    // debugger
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let that = this;
    let config = {
      //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: that.autocomplete.query,
      componentRestrictions: {}
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      console.log("lat long not find ", predictions);

      that.autocompleteItems = [];
      predictions.forEach(function (prediction) {
        that.autocompleteItems.push(prediction);
      });
      console.log("autocompleteItems=> " + that.autocompleteItems)
    });
  }

  chooseItem(item) {
    let that = this;
    that.autocomplete.query = item.description;
    console.log("console items=> " + JSON.stringify(item))
    that.autocompleteItems = [];

    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.forwardGeocode(item.description, options)
      .then((coordinates: NativeGeocoderForwardResult[]) => {
        console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude)
        that.newLat = coordinates[0].latitude;
        that.newLng = coordinates[0].longitude;
        let pos: CameraPosition<ILatLng> = {
          target: new LatLng(that.newLat, that.newLng),
          zoom: 15,
          tilt: 30
        };
        this.map.moveCamera(pos);
        this.markerObj.remove();
        this.map.addMarker({
          title: 'Add POI here(Drag Me)',
          position: new LatLng(that.newLat, that.newLng),
          draggable: true
        }).then((data: Marker) => {
          console.log("Marker added")
          // this.markerObj = marker;
          data.on(GoogleMapsEvent.MARKER_DRAG_END)
            .subscribe(() => {
              // this.markerObj = marker.getPosition();
              that.newLat = data.getPosition().lat;
              that.newLng = data.getPosition().lng;
            });
        })

      })
      .catch((error: any) => console.log(error));

  }

  createPOI() {
    if (this.poi_name == undefined || this.radius == undefined) {
      let alert = this.alertCtrl.create({
        message: "Please fill all the fields first and try again..",
        buttons: ["Okay"]
      });
      alert.present();
    } else {
      let that = this;
      if (this.newLat == undefined || this.newLng == undefined) {
        this.newLat = that.markerObj.getPosition().lat;
        this.newLng = that.markerObj.getPosition().lng;
        let options: NativeGeocoderOptions = {
          useLocale: true,
          maxResults: 5
        };
        this.nativeGeocoder.reverseGeocode(this.newLat, this.newLng, options)
          .then((result: NativeGeocoderReverseResult[]) => {
            console.log("reverse geocode: " + JSON.stringify(result[0]));
            var addressStr = result[0].locality + ' ' + result[0].subLocality + ' ' + result[0].subAdministrativeArea + ' ' + result[0].countryName;
            this.autocomplete.query = "";
            this.autocomplete.query = addressStr;
            var payload = {
              "poi": [
                {
                  "location": {
                    "type": "Point",
                    "coordinates": [
                      this.newLng,
                      this.newLat
                    ]
                  },
                  "poiname": that.poi_name,
                  "status": "Active",
                  "user": that.islogin._id,
                  "address": this.autocomplete.query,
                  "radius": that.radius
                }
              ]
            };
            var _baurl = this.urls.mainURL + "/poi/addpoi";
            this.apicall.startLoading().present();
            this.apicall.urlwithPayload(_baurl, payload)
              .subscribe(data => {
                console.log("response adding poi: ", data)
                this.apicall.stopLoading();
                this.events.publish('reloadpoilist');
                let toast = this.toastCtrl.create({
                  message: "POI added successfully!",
                  duration: 2000,
                  position: 'top'
                });
                toast.present();
                this.navCtrl.pop();
              },
                err => {
                  console.log("error adding poi: ", err);
                  this.apicall.stopLoading();
                })
          })
          .catch((error: any) => console.log(error));
      } else {
        var payload = {
          "poi": [
            {
              "location": {
                "type": "Point",
                "coordinates": [
                  that.newLng,
                  that.newLat
                ]
              },
              "poiname": that.poi_name,
              "status": "Active",
              "user": that.islogin._id,
              "address": that.autocomplete.query,
              "radius": that.radius
            }
          ]
        };
        var bur = this.urls.mainURL + "/poi/addpoi";
        this.apicall.startLoading().present();
        this.apicall.urlwithPayload(bur, payload)
          .subscribe(data => {
            console.log("response adding poi: ", data)
            this.apicall.stopLoading();
            this.events.publish('reloadpoilist');
            let toast = this.toastCtrl.create({
              message: "POI added successfully!",
              duration: 2000,
              position: 'top'
            });
            toast.present();
            this.navCtrl.pop();
          },
            err => {
              console.log("error adding poi: ", err);
              this.apicall.stopLoading();
            })
      }


    }

  }

  getdevices() {
    this.apicall.startLoading().present();
    this.apicall.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apicall.stopLoading();
        this.portstemp = data.devices;
        for (var i = 0; i < data.devices.length; i++) {
          this.arrOBJ.push(data.devices[i]._id);
        }
      },
        err => {
          this.apicall.stopLoading();
          console.log(err);
        });
  }

  onchangeeev(ev) {
    console.log("ev: " + ev)
    this.changedEvent = undefined;
    this.changedEvent = ev;
  }

  update() {
    let that = this;
    this.sortedArr = [];
    this.beforSortArr = [];
    console.log("array obj list: ", JSON.stringify(this.arrOBJ));
    // console.log("selected array: ", JSON.stringify(that.changedEvent))
    var bsutl = this.urls.mainURL + "/vehtra/poi/updatePOI";
    debugger
    if (this.newLat == undefined || this.newLng == undefined) {
      this.newLat = that.markerObj.getPosition().lat;
      this.newLng = that.markerObj.getPosition().lng;
      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
      };
      this.nativeGeocoder.reverseGeocode(this.newLat, this.newLng, options)
        .then((result: NativeGeocoderReverseResult[]) => {
          console.log("reverse geocode: " + JSON.stringify(result[0]));
          var addressStr = result[0].locality + ' ' + result[0].subLocality + ' ' + result[0].subAdministrativeArea + ' ' + result[0].countryName;
          this.autocomplete.query = "";
          this.autocomplete.query = addressStr;
          var pay = {};
          
          if (this.changedEvent != undefined) {
            for (var t = 0; t < this.portstemp.length; t++) {
              for (var r = 0; r < this.changedEvent.length; r++) {
                if (this.changedEvent[r] == this.portstemp[t].Device_Name) {
                  this.beforSortArr.push(this.portstemp[r]._id);
                }
              }
            }
            for (var rt = 0; rt < this.arrOBJ.length; rt++) {
              var myArray = this.beforSortArr,
                needle = this.arrOBJ[rt],
                index = this.contains.call(myArray, needle); // true
              if (!index) {
                this.sortedArr.push(this.arrOBJ[rt]);
              }
            }

            console.log("sorted array: ", JSON.stringify(this.sortedArr))
            pay = {
              "location": {
                "type": "Point",
                "coordinates": [
                  this.newLng,
                  this.newLat
                ]
              },
              "poiid": this.navParameters._id,
              "address": this.autocomplete.query,
              "radius": this.radius,
              "deviceArr": this.sortedArr
            }
          } else {
            pay = {
              "location": {
                "type": "Point",
                "coordinates": [
                  this.newLng,
                  this.newLat
                ]
              },
              "poiid": this.navParameters._id,
              "address": this.autocomplete.query,
              "radius": this.radius,
              "deviceArr": []
            }
          }

          this.apicall.startLoading().present();
          this.apicall.urlwithPayload(bsutl, pay)
            .subscribe(data => {
              console.log("response adding poi: ", data)
              this.apicall.stopLoading();
              this.events.publish('reloadpoilist');
              let toast = this.toastCtrl.create({
                message: "POI updated successfully!",
                duration: 2000,
                position: 'top'
              });
              toast.present();
              this.navCtrl.pop();
            },
              err => {
                console.log("error adding poi: ", err);
                this.apicall.stopLoading();
              })
        })
        .catch((error: any) => console.log(error));
    } else {
      var pay = {};
      if (this.changedEvent != undefined) {
        for (var t = 0; t < this.portstemp.length; t++) {
          for (var r = 0; r < this.changedEvent.length; r++) {
            if (this.changedEvent[r] == this.portstemp[t].Device_Name) {
              this.beforSortArr.push(this.portstemp[r]._id);
            }
          }
        }
        for (var rt = 0; rt < this.arrOBJ.length; rt++) {
          var myArray = this.beforSortArr,
            needle = this.arrOBJ[rt],
            index = this.contains.call(myArray, needle); // true
          if (!index) {
            this.sortedArr.push(this.arrOBJ[rt]);
          }
        }
        pay = {
          "location": {
            "type": "Point",
            "coordinates": [
              that.newLng,
              that.newLat
            ]
          },
          "poiid": that.navParameters._id,
          "address": that.autocomplete.query,
          "radius": that.radius,
          "deviceArr": this.sortedArr
        }
      } else {
        pay = {
          "location": {
            "type": "Point",
            "coordinates": [
              that.newLng,
              that.newLat
            ]
          },
          "poiid": that.navParameters._id,
          "address": that.autocomplete.query,
          "radius": that.radius,
          "deviceArr": []
        }
      }

      this.apicall.startLoading().present();
      this.apicall.urlwithPayload(bsutl, pay)
        .subscribe(data => {
          console.log("response adding poi: ", data)
          this.apicall.stopLoading();
          this.events.publish('reloadpoilist');
          let toast = this.toastCtrl.create({
            message: "POI updated successfully!",
            duration: 2000,
            position: 'top'
          });
          toast.present();
          this.navCtrl.pop();
        },
          err => {
            console.log("error adding poi: ", err);
            this.apicall.stopLoading();
          })
    }

  }


}
