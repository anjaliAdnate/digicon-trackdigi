import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GoogleMaps } from '@ionic-native/google-maps';
import { URLS } from '../../providers/urls';

@IonicPage()
@Component({
  selector: 'page-poi-list',
  templateUrl: 'poi-list.html',
})
export class PoiListPage {
  islogin: any;
  poilist: any[] = [];
  allData: any = {};
  mapid: any;
  stdid: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public urls: URLS,
    public events: Events) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.events.subscribe('reloadpoilist', () => {
      this.poilist = [];
      this.getpois();
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PoiListPage');
  }

  ngOnInit() {
    this.getpois();
  }

  getpois() {
    var url = this.urls.mainURL + "/poi/getPois?user=" + this.islogin._id;
    this.apiCall.urlPassed(url)
      .subscribe(data => {
        this.poilist = data;
        this.allData._poiListData = [];
        var i = 0, howManyTimes = data.length;
        let that = this;
        function f() {
          that.allData._poiListData.push({
            "poiname": data[i].poi.poiname,
            "radius": data[i].radius ? data[i].radius : 'N/A',
            "address": data[i].poi.address ? data[i].poi.address : 'N/A'
          });

          that.allData._poiListData[that.allData._poiListData.length - 1].mapid = "a" + i;
          that.allData.map = GoogleMaps.create(that.allData._poiListData[that.allData._poiListData.length - 1].mapid, {
            camera: {
              target: {
                lat: data[i].poi.location.coordinates[1],
                lng: data[i].poi.location.coordinates[0]
              },
              zoom: 12
            }
          })

          that.allData.map.addMarker({
            position: {
              lat: data[i].poi.location.coordinates[1],
              lng: data[i].poi.location.coordinates[0]
            }
          })

          i++;
          if (i < howManyTimes) {
            setTimeout(f, 200);
          }
        } f();
      },
        err => {
          console.log("error in pois: ", err)
        })
  }

  editPOI(data) {
    console.log("edited data: " + JSON.stringify(data));
    this.navCtrl.push('AddPoiPage', {
      "param": data
    });
  }

  deletePOI(data) {
    console.log("delete data: ", data)
    let alert = this.alertCtrl.create({
      message: "Do you want to delete this POI?",
      buttons: [{
        text: 'BACK'
      }, {
        text: 'YES PROCEED',
        handler: () => {
          var butr = this.urls.mainURL + "/poi/deletePoi?_id=" + data._id;
          this.apiCall.startLoading().present();
          this.apiCall.urlPassed(butr)
            .subscribe(data => {
              this.apiCall.stopLoading();
              let toast = this.toastCtrl.create({
                message: "POI deleted successfully!",
                position: "top",
                duration: 2000
              })
              toast.present();
              this.getpois();
            },
              err => {
                this.apiCall.stopLoading();
                console.log("error occured while deleting POI: ", err)
                let toast = this.toastCtrl.create({
                  message: "POI deleted successfully!",
                  position: "top",
                  duration: 2000
                })
                toast.present();
                this.getpois();
              })
        }
      }]
    });
    alert.present();
  }

  addPOI() {
    this.navCtrl.push('AddPoiPage');
  }

}
