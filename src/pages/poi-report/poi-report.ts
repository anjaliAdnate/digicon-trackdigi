import { OnInit, Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import * as moment from 'moment';
import { URLS } from "../../providers/urls";
declare var google: any;

@IonicPage()
@Component({
    selector: 'page-report-poi',
    templateUrl: './poi-report.html'
})
export class POIReportPage implements OnInit {
    islogin: any;
    portstemp: any[] = [];
    datetimeStart: any;
    datetimeEnd: any;
    poilist: any[] = [];
    // poilist: any;
    selectedPOI: any;
    selectedVehicle: any;
    pageNo: number = 0;
    poireportData: any[] = [];
    locadd: any;
    poiId: any;
    deviceid: any;

    constructor(
        public navCtrl: NavController,
        public navParam: NavParams,
        public apicall: ApiServiceProvider,
        public urls: URLS
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = moment({ hours: 0 }).format();
        console.log('start date', this.datetimeStart)
        this.datetimeEnd = moment().format();//new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }

    ngOnInit() {
        this.getdevices();
        this.getpois();
    }

    getpois() {
        var burl = this.urls.mainURL + "/poi/getPois?user=" + this.islogin._id;
        this.apicall.urlPassed(burl)
            .subscribe(data => {

                for (var i = 0; i < data.length; i++) {
                    this.poilist.push({
                        poiname: data[i].poi.poiname,
                        _id: data[i]._id
                    });
                }

                console.log("poi list: ", this.poilist);
            },
                err => {
                    console.log("error in pois: ", err)
                })
    }

    getdevices() {
        var baseURLp = this.urls.mainURL + '/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        } else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicall.startLoading().present();
        // this.apicall.livedatacall(this.islogin._id, this.islogin.email)
        this.apicall.urlPassed(baseURLp)
            .subscribe(data => {
                this.apicall.stopLoading();
                this.portstemp = data.devices;
            },
                err => {
                    this.apicall.stopLoading();
                    console.log(err);
                });
    }

    getpoiid(selectedPOI) {
        console.log("selectedPOI: ", selectedPOI)
        this.poiId = selectedPOI._id;
    }

    getvehicleid(selectedVehicle) {
        console.log("selectedVehicle: ", selectedVehicle)
        this.deviceid = selectedVehicle._id;
    }

    getReport() {
        var burl = this.urls.mainURL + "/poi/poiReport?user=" + this.islogin._id + "&s=" + this.pageNo + "&l=9&from=" + new Date(this.datetimeStart).toISOString() + "&to=" + new Date(this.datetimeEnd).toISOString() + "&poi=" + this.poiId + "&device=" + this.deviceid;
        this.apicall.startLoading().present();
        this.apicall.urlPassed(burl)
            .subscribe((data) => {
                this.apicall.stopLoading();
                var i = 0, howManyTimes = data.length;
                let that = this;
                that.poireportData = [];
                that.locadd = undefined;
                function f() {
                    that.poireportData.push(
                        {
                            'poiname': data[i].poi.poi.poiname,
                            'Device_Name': data[i].device.Device_Name,
                            'Device_ID': data[i].device.Device_ID,
                            'arrivalTime': data[i].arrivalTime,
                            'departureTime': data[i].departureTime
                        });

                    if (data[i].lat != null && data[i].long != null) {
                        var latEnd = data[i].lat;
                        var lngEnd = data[i].long;
                        var latlng = new google.maps.LatLng(latEnd, lngEnd);
                        var geocoder = new google.maps.Geocoder();
                        var request = {
                            latLng: latlng
                        };
                        geocoder.geocode(request, function (data, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (data[1] != null) {
                                    that.locadd = data[1].formatted_address;
                                }
                            }
                            that.poireportData[that.poireportData.length - 1].address = that.locadd;
                        })
                    }
                    i++;
                    if (i < howManyTimes) {
                        setTimeout(f, 100);
                    }
                }
                f();
                console.log("poireport data: ", that.poireportData);
            },
                err => {
                    this.apicall.stopLoading();
                    console.log("error occured: ", err);
                })
    }
}