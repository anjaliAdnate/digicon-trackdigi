import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowGeofencePage } from './show-geofence';

@NgModule({
  declarations: [
    ShowGeofencePage,
  ],
  imports: [
    IonicPageModule.forChild(ShowGeofencePage),
  ],
})
export class ShowGeofencePageModule {}
